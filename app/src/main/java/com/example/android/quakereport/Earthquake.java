package com.example.android.quakereport;

public class Earthquake {

    private double mMagnitudo;
    private String mCity;
    private String mTime;
    private long mTimeInMilliseconds;
    private String mUrl;



    public Earthquake(Double magnitudo, String city, long timeInMilliseconds,String url){
        mMagnitudo = magnitudo;
        mCity = city;
        mTimeInMilliseconds = timeInMilliseconds;
        mUrl = url;
    }

    public Double getmMagnitudo() {
        return mMagnitudo;
    }

    public String getmCity() {
        return mCity;
    }

    public String getmTime() {
        return mTime;
    }

    public long getmTimeInMilliseconds() {
        return mTimeInMilliseconds;
    }

    public String getmUrl() {
        return mUrl;
    }
}
