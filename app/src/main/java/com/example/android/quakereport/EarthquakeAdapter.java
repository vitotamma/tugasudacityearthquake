package com.example.android.quakereport;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EarthquakeAdapter extends ArrayAdapter<Earthquake> {
    public EarthquakeAdapter(Activity context, ArrayList<Earthquake> earthquakes) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, earthquakes);
    }

    String primaryLocation;
    String locationOffset;
    int magnitude1Color = ContextCompat.getColor(getContext(), R.color.magnitude1);


    private static final String LOCATION_SEPARATOR = " of ";

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_layout, parent, false);
        }

        Earthquake currentEarthquake = getItem(position);

        TextView magnitudoTextView = listItemView.findViewById(R.id.TVMagnitudo);
        String formattedMagnitude = formatMagnitude(currentEarthquake.getmMagnitudo());
//        magnitudoTextView.setText(currentEarthquake.getmMagnitudo());
        magnitudoTextView.setText(formattedMagnitude);

        //Atur warna latar belakang di lingkaran magnitudo.
        // Ambil latar belakang dari TextView, yang berupa GradientDrawable.
        GradientDrawable magnitudeCircle = (GradientDrawable) magnitudoTextView.getBackground();

        // Ambil warna latar belakang sesuai magnitudo saat itu
        int magnitudeColor = getMagnitudeColor(currentEarthquake.getmMagnitudo());

        // Atur warna lingkaran magnitudo
        magnitudeCircle.setColor(magnitudeColor);

//        TextView cityTextView = listItemView.findViewById(R.id.TVCity);
//        cityTextView.setText(currentEarthquake.getmCity());

        Date dateObject = new Date(currentEarthquake.getmTimeInMilliseconds());

        TextView timeTextView = listItemView.findViewById(R.id.TVTime);
        String formattedTime = formatTime(dateObject);
        timeTextView.setText(formattedTime);

        TextView jamTrextView = listItemView.findViewById(R.id.TVJam);
        String formattedJam = formatJam(dateObject);
        jamTrextView.setText(formattedJam);

        String originalLocation = currentEarthquake.getmCity();
        if (originalLocation.contains(LOCATION_SEPARATOR)) {
            String[] parts = originalLocation.split(LOCATION_SEPARATOR);
            locationOffset = parts[0] + LOCATION_SEPARATOR;
            primaryLocation = parts[1];
        } else {
            locationOffset = getContext().getString(R.string.near_the);
            primaryLocation = originalLocation;
        }

        TextView primaryLocationView = (TextView) listItemView.findViewById(R.id.TVCity);
        primaryLocationView.setText(primaryLocation);

        TextView locationOffsetView = (TextView) listItemView.findViewById(R.id.TVSpecific);
        locationOffsetView.setText(locationOffset);



        return listItemView;
    }
    /**
     * Kembalikan string tanggal terformat ("Mar 3, 1984") dari objek Date.
     */
    private String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("LLL dd, yyyy");
        return timeFormat.format(dateObject);
    }

    /**
     * Kembalikan string tanggal terformat ( "4:30 PM") dari objek Date.
     */
    private String formatJam(Date dateObject) {
        SimpleDateFormat jamFormat = new SimpleDateFormat("h:mm a");
        return jamFormat.format(dateObject);
    }

    private String formatMagnitude(double magnitude) {
        DecimalFormat magnitudeFormat = new DecimalFormat("0.0");
        return magnitudeFormat.format(magnitude);
    }
    private int getMagnitudeColor(double magnitude) {
        int magnitudeColorResourceId;
        int magnitudeFloor = (int) Math.floor(magnitude);
        switch (magnitudeFloor) {
            case 0:
            case 1:
                magnitudeColorResourceId = R.color.magnitude1;
                break;
            case 2:
                magnitudeColorResourceId = R.color.magnitude2;
                break;
            case 3:
                magnitudeColorResourceId = R.color.magnitude3;
                break;
            case 4:
                magnitudeColorResourceId = R.color.magnitude4;
                break;
            case 5:
                magnitudeColorResourceId = R.color.magnitude5;
                break;
            case 6:
                magnitudeColorResourceId = R.color.magnitude6;
                break;
            case 7:
                magnitudeColorResourceId = R.color.magnitude7;
                break;
            case 8:
                magnitudeColorResourceId = R.color.magnitude8;
                break;
            case 9:
                magnitudeColorResourceId = R.color.magnitude9;
                break;
            default:
                magnitudeColorResourceId = R.color.magnitude10plus;
                break;
        }
        return ContextCompat.getColor(getContext(), magnitudeColorResourceId);
    }
}
